// ENUM
public enum Suit {
  HEARTS(0.4),
  SPADES(0.3),
  DIAMONDS(0.2),
  CLUBS(0.1);

  // field
  private double suitScore;

  // constructor
  //in class, she said that constructors are private for ENUMS
  private Suit(double score) {
    this.suitScore = score;
  }

  // getter
  public double getScore() {
    return this.suitScore;
  }

}
