import java.util.Random;

public class Deck {
  // fields
  private Card[] cards;
  private int numberOfCards;
  private Random rng;

  // getters
  public Card[] getCards() {
    return this.cards;
  }

  public int getNumberOfCards() {
    return this.numberOfCards;
  }

  public Random getRng() {
    return this.rng;
  }

  // constructor
  public Deck() {
    this.numberOfCards = 52;
    this.rng = new Random(); // remove Random in front of rng
    cards = new Card[numberOfCards]; // remove (Card[]) in front of cards

    // change this with enum using for each loop
    int counter = 0;
    for (Suit theSuit : Suit.values()) {
      for (Rank theRank : Rank.values()) {
        Card temp = new Card(theSuit, theRank);
        cards[counter] = temp;
        counter++;
      }
    }
  }

  // custom instance methods

  public int length() {
    return numberOfCards;
  }

  public Card drawTopCard() {
    this.numberOfCards = this.numberOfCards - 1;
    return cards[this.numberOfCards]; // i removed -1 (supposed to return the card in the last position) here in the
                                      // [] and it doesn't throw anymore exceptions
  }

  public void shuffle() {
    for (int i = 0; i < this.numberOfCards; i++) {
      int rngNum = rng.nextInt(this.numberOfCards);
      Card temp = cards[i];
      this.cards[i] = cards[rngNum];
      cards[rngNum] = temp;
      System.out.println(this.cards[i]); // keep this line to visual the SHUFFLING
    }
  }

  public String toString() {
    String deck = "";
    for (int i = 0; i < numberOfCards; i++) {
      deck = deck + this.cards[i] + "\n";
    }
    return deck;
  }

}
