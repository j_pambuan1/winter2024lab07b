public class SimpleWar {
  public static void main(String[] args) {
    Deck myDeck = new Deck();
    myDeck.shuffle();

    int playerOneTotalPoints = 0;
    int playerTwoTotalPoints = 0;
	int i = 0; //to initialize the round
	
    while (myDeck.length() > 0) {

      Card firstCard = myDeck.drawTopCard();
	  System.out.println("\u001B[33m----------------ROUND " + i + "-------------------- \u001B[0m");
      System.out.println("First player card drawn: " + firstCard);
      Card secondCard = myDeck.drawTopCard();
      System.out.println("Second player card drawn: " + secondCard);
      System.out.println("\033[31m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \033[0m");
      System.out.println("Player 1 points before the game: " + playerOneTotalPoints);
      System.out.println("Player 2 points before the game: " + playerTwoTotalPoints);
      System.out.println("\033[31m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \033[0m");
      double playerOnePts = firstCard.calculateScore();
      double playerTwoPts = secondCard.calculateScore();
      System.out.println("Player 1 points : " + playerOnePts + " " + "(" + firstCard + ")");
      System.out.println("Player 2 points: " + playerTwoPts + " " + "(" + secondCard + ")");
      System.out.println("\033[31m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \033[0m");

      if (playerOnePts > playerTwoPts) {
        playerOneTotalPoints++;
        System.out.println("Player 2 current total points: " + playerOneTotalPoints);
      } else {
        playerTwoTotalPoints++;
        System.out.println("Player 2 current total points: " + playerTwoTotalPoints);
      }
		i++;
    }
    if (playerOneTotalPoints > playerTwoTotalPoints) {
      System.out.println("Congratulations, player 1! You won!");
    } else if (playerTwoTotalPoints > playerOneTotalPoints) {
      System.out.println("Congratulations, player 2! You won!");
    } else {
      System.out.println("Draw!");
    }
  }
}
