public class Card {
  // fields
  private Suit suits; // Hearts, Diamonds, Spades, Clubs
  private Rank value; // Ace, Two, Three... Ten, Jack, King, Queen

  // constructor
  public Card(Suit suits, Rank value) {
    this.suits = suits;
    this.value = value;
  }

  // getters
  public Suit getSuit() {
    return this.suits;
  }

  public Rank getValue() {
    return this.value;
  }

  public String toString() {
    return this.value + " of " + this.suits;
  }

  public double calculateScore() {
    double score = this.value.getScore();
    double suitScore = this.suits.getScore();
    // for visual
    //System.out.println("score: " + score + " suitScore: " + suitScore);
    return score + suitScore;
  }
}
